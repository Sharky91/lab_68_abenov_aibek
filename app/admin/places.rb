ActiveAdmin.register Place do

	form do |f|
		f.inputs do
			f.input :name
			f.input :desc
			if f.object.image.attached?
				f.input :image,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.image.variant(combine_options: 
							{ gravity: 'Center', crop: '50x50+0+0' })
						)
					)
			else
				f.input :image, :as => :file
			end
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :image do |place|
				if place.image.attached?
				image_tag place.image.variant(combine_options: 
					{ gravity: 'Center', crop: '50x50+0+0' })
			end
		end
		column :place do |place|
			link_to place.name, admin_place_path(place)
			
		end
		actions
	end

	show do
		attributes_table do
			row :image do |place|
				image_tag place.image.variant(combine_options: 
				{ 
					gravity: 'Center', crop: '50x50+0' 
					})
			end
			row :name
			row :desc
		end
		active_admin_comments
	end



	permit_params :name, :desc, :image
end
