ActiveAdmin.register Dish do

	form do |f|
		f.inputs do
			f.input :place
			f.input :name
			f.input :desc
			f.input :price
			if f.object.picture.attached?
				f.input :picture,
				:as => :file,
				:hint => image_tag(
					url_for(
						f.object.picture.variant(combine_options:
							{ gravity: 'Center', crop: '50x50+0+0' }
							)
						)
					)
			else
				f.input :picture, :as => :file
			end
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :picture do |dish|
			image_tag dish.picture.variant(combine_options: 
				{ gravity: 'Center', crop: '50x50+0+0' })
		end
		column :dish do |dish|
			link_to dish.name, admin_dish_path(dish)
		end
		column :price
		column :place
		actions
	end

	show do
		attributes_table do
			row :image do |dish|
				image_tag dish.picture.variant(combine_options: 
				{ 
					gravity: 'Center', crop: '50x50+0' 
					})
			end
			row :name
			row :place
			row :price
			row :desc
		end
		active_admin_comments
	end



	permit_params :place_id, :name, :desc, :price, :picture

end
