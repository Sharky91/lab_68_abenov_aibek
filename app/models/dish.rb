class Dish < ApplicationRecord
  belongs_to :place
  has_one_attached :picture
	validates :picture, 
	 file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }
end
