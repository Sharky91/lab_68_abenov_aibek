class Place < ApplicationRecord
	has_many :dishes
	has_one_attached :image
	validates :image, 
	 file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

end
